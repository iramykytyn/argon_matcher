
CPP=g++
LDFLAGS=-g
INCLUDE=-I./argon2/include
ARGON2_LIB=./argon2/libargon2.a -lpthread


all: matcher generator

matcher: argon_matcher

argon_matcher: main.o convert.o
	( cd argon2 ; make libs)
	$(CPP) $(LDFLAGS) -o argon_matcher main.o convert.o $(ARGON2_LIB)

convert.o: convert.cpp
	$(CPP) $(LDFLAGS) -c convert.cpp $(INCLUDE)

main.o: main.cpp
	$(CPP) $(LDFLAGS) -c main.cpp $(INCLUDE)


generator: generate_hash

generate_hash: generate_hash.o convert.o
	( cd argon2 ; make libs)
	$(CPP) $(LDFLAGS) -o generate_hash generate_hash.o convert.o $(ARGON2_LIB)


generate_hash.o: generate_hash.cpp
	$(CPP) $(LDFLAGS) -c generate_hash.cpp $(INCLUDE)


clean:
	rm -rf argon_matcher
	rm -rf *.o
	rm -rf generate_hash
	rm -rf ./argon2/libargon2.a
