#include <iostream>
#include <string>
#include <math.h>
#include <sstream>
#include <cstring>
#include <argon2.h>
#include <ctime>
#include <iomanip>

#include "convert.h"

#define HASHLEN 64
#define SALTLEN 24

static const uint32_t t_cost = 1;            // 1 iteration
static const uint32_t m_cost = 1<<18;        // 256 (2^18) mebibytes memory usage
static const uint32_t parallelism = 1;       // number of threads and lanes
static const std::string salt_param = "c7f232491f3c7d0caa44e312150866eb9bf7fc3ec0d7fe9e";


int main(int argc, char *argv[])
{

  if(argc != 2)
  {
    std::cerr << "usage: myapp password" << std::endl;
    return false;
  }


  std::string pass_word = argv[1];
  if(!isValid(pass_word, 128))
  {
    std::cerr << "Error : second parameter is not valid, "
                 "please retry or contact support." << std::endl;
    return false;
  }

  // varaibles for byte data
  uint8_t salt[SALTLEN];
  uint8_t pass_bt[HASHLEN];

  // hashresult will be stored it this varaible
  uint8_t hash_result[HASHLEN];

  // Convert the provided parameters from hex to bytes
  hex_to_bytes(salt_param, salt);
  hex_to_bytes(pass_word, pass_bt);

  std::string hash_result_str;

  // variable for storing return code from argon2d_hash_raw function
  int return_code;

  // call : argon2d_hash_raw(t_cost, m_cost, parallelism, pwd, pwdlen, salt, SALTLEN, hash, HASHLEN);
  return_code = argon2d_hash_raw(t_cost, m_cost, parallelism, pass_bt, HASHLEN,
                   salt, SALTLEN, hash_result, HASHLEN);

  if(return_code != ARGON2_OK)
  {
	  std::cerr << "Error : Argon2 failed with error code " << return_code
				<< ". Please contact support" << std::endl;
	  return false;
  }

  bytes_to_hex(hash_result, hash_result_str, HASHLEN);

  if(!isValid(hash_result_str, 128))
  {
    std::cerr << "Error : Argon2 failed, please contact support" << std::endl;
    return false;
  }

  std::cout << "Hash result: " << hash_result_str << std::endl;

  return true;
}


