TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    convert.cpp \
    generate_hash.cpp

DISTFILES += \
    specs20170211.txt

HEADERS += \
    argon2.h \
    convert.h
