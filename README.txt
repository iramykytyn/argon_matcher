
== Installation guide ==

== target : Linux OS 32/64 bits ==
- apt-get install g++ g++-4.9 libstdc++-4.9-dev
- make make matcher
- make generate_hash


== target : Mac OS 32/64 bits ==
You need to have make and gcc installed on your machine, 
if you have not, here you can find short instruction how to do it https://www.cyberciti.biz/faq/howto-apple-mac-os-x-install-gcc-compiler/.

Run

- make matcher

to build utility for password matching, or

- make generate_hash

to build utility for hash generation. Or run

- make

to build both.


