#include <iostream>
#include <argon2.h>
#include <string>
#include <sstream>

// If there is not exactly 124 characters, any character is not hex or
// any character is uppercase hex [A-F] return false
// also upparcase can be alowed by setting third param to true
bool isValid(const std::string &param, const unsigned param_length, bool allow_uppercase = false);

unsigned hex_to_unsigned_int(const std::string &hex);
void decimal_to_hex(const unsigned dec_val, uint8_t* resume);
void hex_to_bytes(const std::string &hex, uint8_t* bytes);
void bytes_to_hex(uint8_t* bytes,
          std::string& hex,
          const size_t size);
