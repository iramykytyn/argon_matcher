#include "convert.h"

bool isValid(const std::string &param, const unsigned param_length, bool allow_uppercase)
{
  if(param.length() != param_length)
    return false;

  for(unsigned i = 0; i < param_length; ++i)
  {
    if( !((param[i] >= '0' && param[i] <= '9')
       ||(param[i] >= 'a' && param[i] <= 'f')
       || (allow_uppercase && (param[i] >= 'A' && param[i] <= 'F'))) )
    {
      return false;
    }
  }
  return true;
}


void hex_to_bytes(const std::string &hex,
          uint8_t* bytes)
{
  std::string two_chars;
  unsigned one_byte;

  for(unsigned i = 0, j = 0; i < hex.length(); ++j)
  {
    two_chars = hex.substr(i, 2);
    std::stringstream ss;
    ss  << two_chars; // std::string hex_value
    ss >> std::hex >> one_byte; //int decimal_value
    bytes[j] = static_cast<uint8_t>(one_byte);
    two_chars.clear();
    i = i + 2;
  }
}

void bytes_to_hex(uint8_t* bytes,
          std::string &hex,
          const size_t size)
{
  for(unsigned i = 0; i < size; ++i)
  {
    std::stringstream ss;
    ss<< std::hex << static_cast<unsigned>(bytes[i]); // decimal value

    if(ss.str().length() == 1)
      hex.append("0");
    hex.append(ss.str());
  }
}


unsigned hex_to_unsigned_int(const std::string &hex)
{
  unsigned long decimal_value;

  std::stringstream ss;
  ss  << hex;						  // std::string hex_value
  ss >> std::hex >> decimal_value ; //int decimal_value

  return decimal_value;
}


