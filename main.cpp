#include <iostream>
#include <string>
#include <math.h>
#include <sstream>
#include <cstring>
#include <argon2.h>
#include <ctime>
#include <iomanip>

#include "convert.h"

#define HASHLEN 64
#define SALTLEN 24

const std::string info_message = "Operations have started : the software may need "
								 "6 hours max to complete, please wait...\n"
								 "Press CTRL+C at any time to stop, "
								 "even if the operations are not finished yet.\n"
								 "A new checkpoint will be displayed every 15 minutes : "
								 "you can resume later by using it as last parameter instead of 0000.\n";


static const uint32_t t_cost = 1;            // 1 iteration
static const uint32_t m_cost = 1<<18;        // 256 (2^18) mebibytes memory usage
static const uint32_t parallelism = 1;       // number of threads and lanes
static const std::string salt_param = "c7f232491f3c7d0caa44e312150866eb9bf7fc3ec0d7fe9e";

int main(int argc, char *argv[])
{

	if(argc != 4)
	{
		std::cerr << "usage: myapp param1 param2 param3" << std::endl;
		return false;
	}

	std::string param1 = argv[1];
	if(!isValid(param1, 124))
	{
		std::cerr << "Error : first parameter is not valid, "
					 "please retry or contact support." << std::endl;
		return false;
	}

	std::string param2 = argv[2];
	if(!isValid(param2, 128))
	{
		std::cerr << "Error : second parameter is not valid, "
					 "please retry or contact support." << std::endl;
		return false;
	}

	std::string param3 = argv[3];
	if(!isValid(param3, 4))
	{
		std::cerr << "Error : third parameter is not valid, "
					 "please retry or contact support." << std::endl;
		return false;
	}

	std::cout << info_message << std::endl;

	// varaibles for byte data
	uint8_t salt[SALTLEN];
	uint8_t pwd[HASHLEN];
	uint8_t clue[HASHLEN - 2];
	uint8_t resume[2];

	// hashresult will be stored it this varaible
	uint8_t hash_result[HASHLEN];

	// Convert the provided parameters from hex to bytes
	hex_to_bytes(salt_param, salt);
	hex_to_bytes(param1, clue);
	hex_to_bytes(param3, resume);

	std::string hash_result_str, resume_str;

	// varaible which represent param3 as unsigned int (2 bytes)
	// used for simplifying incrementing of resume
	unsigned decimal_val = hex_to_unsigned_int(param3);
	memcpy(pwd, clue, HASHLEN - 2);

	do
	{
		hash_result_str.clear();

		// Concatenate "clue" (62 bytes) + "resume" (2 bytes)
		memcpy(pwd + HASHLEN - 2, resume, 2);

		// variable for storing return code from argon2d_hash_raw function
		int return_code;

		// call : argon2d_hash_raw(t_cost, m_cost, parallelism, pwd, pwdlen, salt, SALTLEN, hash, HASHLEN);
		return_code = argon2d_hash_raw(t_cost, m_cost, parallelism, pwd, HASHLEN,
						 salt, SALTLEN, hash_result, HASHLEN);

		if(return_code != ARGON2_OK)
		{
			std::cerr << "Error : Argon2 failed with error code " << return_code
					  << ". Please contact support" << std::endl;
			return false;
		}

		bytes_to_hex(hash_result, hash_result_str, HASHLEN);

		if(!isValid(hash_result_str, 128))
		{
			std::cerr << "Error : Argon2 failed, please contact support" << std::endl;
			return false;
		}

		if((hash_result_str == param2) || decimal_val == 0xffff)
			break;

		// increment resume
		++decimal_val;
		resume[0] = *((unsigned char *)&decimal_val + 1);
		resume[1] = *((unsigned char *)&decimal_val);

		if(!(decimal_val % 2622))
		{
			bytes_to_hex(resume, resume_str, 2);
			std::cout << "Checkpoint reached : " << resume_str << std::endl;
			resume_str.clear();
		}
	} while ( true );

	if (hash_result_str == param2)
	{
		bytes_to_hex(resume, resume_str, 2);
		std::cout << "Operations have finished.\n"
					 "Success ! The result is : " << resume_str << std::endl;
		return true;
	}
	else
	{
		std::cout << "Operations have finished.\n"
					 "Failure ! All possible cases have been tried but none matched,"
					 " please retry or contact support." << std::endl;
		return false;
	}
}


